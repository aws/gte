#include <gte.hpp>

int main(int argc, char **argv)
{
	std::string test_tmpl = "A`{{ magic }}`B C`{* test this *}`D";
	gte::Template *test = new gte::Template((void*)test_tmpl.c_str(),
		test_tmpl.length());

	// Test A`test`B
	test->SetVariable("magic", "test");

	// Test C`...`D
	std::string test_tmpl2 = "`{{ a }}` ``{{ b }}`` ```{{ c }}```\n";
	gte::Template *test_internal = new gte::Template(
		(void*)test_tmpl2.c_str(),
		test_tmpl.length());

	gte::List *this_list = new gte::List();
	gte::Dictionary *this_list_a = new gte::Dictionary();
	this_list_a->Set("a", "A");
	this_list_a->Set("b", "B");
	this_list_a->Set("c", "C");

	gte::Dictionary *this_list_b = new gte::Dictionary();
	this_list_b->Set("a", "1");
	this_list_b->Set("b", "2");
	this_list_b->Set("c", "3");


	gte::Dictionary *this_list_c = new gte::Dictionary();
	this_list_c->Set("a", "x");
	this_list_c->Set("b", "y");
	this_list_c->Set("c", "z");

	this_list->Add(this_list_a);
	this_list->Add(this_list_b);
	this_list->Add(this_list_c);

	test->SetVariable("test", test_internal);

	test->SetVariable("this", this_list);

	test->PrintTokens();

	std::string out;
	test->Process(out);

	printf("Test out:\n```\n%s\n```\n", out.c_str());

	return 0;
}