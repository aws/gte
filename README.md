'gte' General Templating Engine
==================================

'gte', or the General Templating Engine, is my response to growing frustration with attempting to implement, import, export, handle, manager, tweak, frob, or otherwise beat into submission any sort of template engine designed for a specific task when all I actually need is simple find/replace.

This is not self-securing, fast, or anything fancy.  It does its job, it's easy to read, it was easy to code (it took about an hour) and allowed me to replace aging code in various applications with a basic templating system.

Note: This is lightly tested and not 'secure' in a real sense.  I use it for whenever I need to handle templated output which may change, and I trust (or otherwise control) the output.  This is particularly useful for localhost only systems with no user interaction (e.g. decompilers, chunkers, etc.) which need to output consistent (or semi-consistent) data of known types.  Since all data internally is string type or should be loaded as a valid variable it is relatively safe.


Variable Types
-------------------------------

All variables are handled by `gte::Variable`, a simple class which is inherited by the following types:

 * `gte::String`, internally represented by `std::string`;
 * `gte::List`, internally represented by `std::vector<gte::Variable*>`;
 * `gte::Dictionary`, internally represented by `std::map<std::string, gte::Variable*>`;
 * `gte::Template` (inheriting from gte::Variable for the purposes of inclusion, see below).

This simplistic subset of variables allows for the use of whatever type of input/output is desired by the end system without having to worry about internal needs or output concerns.


Syntax
-------------------------------

With simple aims comes simple syntax:

 * `{{ variable }}` a single variable to replace;
 * `{# template_key #}` include a `gte::Template` (stored using template_key) processing it using its internal dictionary;
 * `{* template_string_key list_key *}` uses the `gte::Template` stored using the key `template_string_key` (a string) from parent template's dictionary and iterates over the dictionaries within the `gte::List` (which is the value of the key `list_key`) -- any non-dictionary within the list is ignored.

Note the space either trailing or preceeding the variable are not option, they are used as part of the parsing process (i.e. the tokens are `{{ `, ` }}` and `{* ` and ` *}`).

Additionally note that the `gte::Template` may be loaded using stream or string and may be overloaded using SetDictionary and GetDictionary (to set or back up the existing system) to avoid unnecessary reloads/re-preprocessing.

The syntax may be expanded, see `src/gte.cpp`.


Usage (gte in your project)
-------------------------------

Include the header and source file in your project.  Support for compiling as a Windows DLL is not built in, but with that in mind I did include GTE_API with a few #ifdef checks so that if you require it you may merely define GTE_API with your import/export type prior to including `gte.hpp` for the first time.


Simplest Example Usage (C++)
-------------------------------

See `tests/test0/test0.cpp`.

Licence
------------------------

gte is released into the wild under the new BSD Licence.  See LICENCE for the full text.