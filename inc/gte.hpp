// Copyright (c) 2008-2014 A.W. Stanley. All rights reserved.
// See LICENCE included with gte.
#pragma once

#include <functional>
#include <fstream>
#include <map>
#include <stdint.h>
#include <string>
#include <vector>

namespace gte
{
#pragma region Variable

	//! Variable types (used for switching; add more for your implementation ...)
	enum eVariableType
	{
		eVarNone = 0,
		eVarString,
		eVarList,
		eVarDictionary,
		eVarTemplate
	};

	//! Generic variable
	class Variable
	{
	protected:
		//! Variable type
		eVariableType m_type;
	public:

		//! Create a variable
		Variable();

		//! Destroy the variable
		~Variable();

		//! Get type
		eVariableType GetType();
	};

	class String : public Variable
	{
	protected:
		//! Internal storage of string
		std::string m_value;
	public:

		//! Create a string with an initial value
		/*!
		\param value initial value (default "")
		 */
		String(std::string value="");

		//! Destroy the string
		~String();

		//! Set value
		/*!
		\param value new string value.
		 */
		void Set(std::string value);

		//! Get the value
		/*!
		\return the value of the string
		 */
		std::string Get();
	};

	class List: public Variable
	{
	protected:
		std::vector<Variable*> m_value;
		
		std::vector<Variable*>::iterator m_iterator;

	public:
		//! Create a list
		List();

		//! Destroy list (and all variables within it)
		~List();

		//! Append variable to the list
		/*!
		\param value variable to the appended.
		 */
		void Add(Variable *value);

		//! Get the first entry in the list
		/*!
		\param value a pointer to accept the variable pointer
		\return true on success; false on failure (end of list)
		*/
		bool GetFirst(Variable **value);

		//! Get the next value in the map
		/*!
		\param value a pointer to accept the variable pointer
		\return true on success; false on failure (end of list)
		*/
		bool GetNext(Variable **value);
	};


	//! A dictionary variable type
	class Dictionary : public Variable
	{
	protected:

		//! Actual storage
		std::map<std::string, Variable*> m_value;

		//! Iterator
		std::map<std::string, Variable*>::iterator m_iterator;

	public:

		//! Create a new dictionary
		Dictionary();

		//! Destroy the dictionary, deleting all internal variables
		~Dictionary();

		//! Set a variable's value by key.
		/*!
		\param key Variable 'key'
		\param value Pointer to a valid variable (nullptr produces "")
		*/
		void Set(std::string key,
			Variable *value);

		//! Set a variable's value to a string
		/*!
		\param key Variable key
		\param value string value.
		 */
		void Set(std::string key, std::string value);

		//! Get the first value in the map
		/*!
		\param key the next variable's key
		\param value a pointer to accept the variable pointer
		\return true on success; false on failure
		*/
		bool GetFirst(std::string &key, Variable **value);

		//! Get the next value in the map
		/*!
		\param key the next variable's key
		\param value a pointer to accept the variable pointer
		\return true on success; false on failure
		*/
		bool GetNext(std::string &key, Variable **value);

		//! Get variable by key
		/*!
		\param key target key
		\param value a pointer to accept the variable pointer
		\return true on success; false on failure
		 */
		bool GetByKey(std::string key, Variable **value);

	};

#pragma endregion Variable

#pragma region Template

	//! Variable within a template
	struct TemplateVariable
	{
		//! Variable type (as by 'input') -- tied to the handler in the template
		uint32_t m_type;

		//! Variable start position (the first '{' - 1)
		uint8_t *var_start;

		//! Variable end position (the last '}' } + 1)
		uint8_t *var_end;

		//! Variable name
		std::string name;
	};

	//! Template variable
	/*! This is a function which handles conversion of the template variable
	    to the variable.

		\param TemplateVariable pointer to a variable in a set of data.
		\param Variable pointer to an array of variables.
		\param std::string& temporary buffer for output
		\return return true on success, false on failure.
	 */
	typedef std::function <
		bool(
			TemplateVariable*,
			Variable**,
			std::string&
		)> TemplateVariableHandler;

	//! A wrapper containing various pieces of information
	struct TemplateHandler
	{
		//! Token for opening the variable
		uint8_t *token_beg;

		//! Token length for opening
		uint32_t toksz_beg;

		//! Token for closing the variable
		uint8_t *token_end;

		//! Token length for closing
		uint32_t toksz_end;

		//! Template variable handler
		TemplateVariableHandler *handler;

		//! Flags (for now only 0/1 for 'in use')
		uint8_t flags;

		//! Construct a template handler
		TemplateHandler(uint8_t *beg, uint32_t szb, uint8_t *end, uint32_t sze,
			TemplateVariableHandler *hndl, uint8_t flags);

		//! Destroy the template handler (deleting tokens and the handler function instance)
		~TemplateHandler();
	};

	//! Alias for template handlers
	typedef std::vector<TemplateHandler*> TemplateHandlers;

	//! Template instance
	/*! A gte Template is the basis for all processing, variable swapping,
	    and data handling.
	 */
	class Template : public Variable
	{
	protected:

		//! Internal map (dictionary)
		Dictionary* m_variables;

		//! Internal token array
		std::vector<TemplateVariable*> m_tokens;

		//! Buffer
		uint8_t* m_buffer;

		//! Full buffer size
		uint64_t m_buffer_size;

		//! Filename (for abstraction purposes)
		std::string m_filename;

		//! Extract the TemplateVariable array
		void PreProcess();

		//! Token open tag
		TemplateHandlers* m_template_handlers;

	public:
		//! Construct the template using a string
		/*!
		\param data template data
		\param uint64_t length
		\param template_handlers array of template handlers (nullptr will load defaults)
		*/
		Template(void *data, uint64_t length,
			TemplateHandlers* template_handlers = nullptr);

		//! Construct the template buffering file from disk
		/*! 
		\param filename filename of the template.
		\param template_handlers array of template handlers (nullptr will load defaults)
		 */
		Template(std::string filename,
			TemplateHandlers* template_handlers = nullptr);

		//! Destroy the template (removing all variables set inside)
		~Template();

		//! Process template to an output stream
		/*!
		\param out output stream (written to based on its current position)
		\return true on success; false on failure
		*/
		bool Process(std::ofstream &out);

		//! Clobber the dictionary (used for template loading)
		/*! 
		\param dict pointer to the new (replacement) dictionary.
		 */
		void SetDictionary(Dictionary *dict);

		//! Get the existing dictionary
		Dictionary* GetDictionary();

		//! Process template to an output string
		/*!
		\param out output string (data is appended to it)
		\return true on success; false on failure
		*/
		bool Process(std::string &out);

		//! Set a variable's value by key.
		/*!
		\param key Variable 'key'
		\param value Pointer to a valid variable (nullptr produces "")
		*/
		void SetVariable(std::string key,
			Variable *value);

		//! Create (and set) a string variable by key.
		/*!
		\param key Variable 'key'
		\param value string (which will be converted into a variable)
		*/
		void SetVariable(std::string key,
			std::string value);

		//! Get a variable's value from its key.
		/*!
		\param key Variable 'key'
		\return Pointer to a valid variable (nullptr produces "")
		*/
		Variable* GetVariable(std::string key);

		//! Print all detected tokens
		void PrintTokens();
	};
#pragma endregion Template
} // namespace gte