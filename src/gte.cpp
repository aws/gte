// Copyright (c) 2008-2014 A.W. Stanley. All rights reserved.
// See LICENCE included with gte.

#include <gte.hpp>

// memcpy
#include <string.h>

namespace gte
{
#pragma region DefaultHandlers

	bool DefaultDoubleBraceHandler(TemplateVariable *T,
		Variable **V, std::string &O)
	{
		if (V == nullptr || T == nullptr)
		{
			return false;
		}

		bool rv = false;

		// Reset out
		O = "";

		switch (V[0]->GetType())
		{
		case eVarString: {
			O += reinterpret_cast<String*>(V[0])->Get();
			rv = true;
		} break;

		case eVarList: {
			Variable *v = nullptr;
			if (reinterpret_cast<List*>(V[0])->GetFirst(&v))
			{
				if (v->GetType() == eVarString)
				{
					O += reinterpret_cast<String*>(v)->Get();
					rv = true;
				}
			}
		} break;

		case eVarDictionary: {
			std::string key = "";
			Variable *v = nullptr;
			if (reinterpret_cast<Dictionary*>(V[0])->GetFirst(key, &v))
			{
				if (v->GetType() == eVarString)
				{
					O += reinterpret_cast<String*>(v)->Get();
					rv = true;
				}
			}
		} break;

		case eVarTemplate: {
			// do nothing
		} break;

		default: break;
		}

		return rv;
	}

	bool DefaultHashHandler(TemplateVariable *T,
		Variable **V, std::string &O)
	{
		if (V == nullptr || T == nullptr)
		{
			return false;
		}

		bool rv = false;

		// Reset out
		O = "";

		Template *key = nullptr;

		// Key
		switch (V[0]->GetType())
		{
		case eVarTemplate: {
			key = reinterpret_cast<Template*>(V[0]);
		} break;

		default:
			break;
		}

		if (key != nullptr)
		{
			std::string tmp = "";
			Variable *v = nullptr;
			rv = key->Process(tmp);
			O += tmp;
			tmp = "";
		}

		return rv;
	}

	bool DefaultAsteriskHandler(TemplateVariable *T,
		Variable **V, std::string &O)
	{
		if (V == nullptr || T == nullptr)
		{
			return false;
		}

		bool rv = false;

		// Reset out
		O = "";

		Template *key = nullptr;
		List *val = nullptr;

		// Key
		switch (V[0]->GetType())
		{
		case eVarTemplate: {
			key = reinterpret_cast<Template*>(V[0]);
		} break;

		default:
			break;
		}

		// Value
		switch (V[1]->GetType())
		{
		case eVarList: {
			val = reinterpret_cast<List*>(V[1]);
		} break;


		default:
			break;
		}

		if (key != nullptr && val != nullptr)
		{
			std::string tmp = "";
			Dictionary *d = key->GetDictionary();
			Dictionary *D = nullptr;
			Variable *v = nullptr;
			if (val->GetFirst(&v))
			{
				D = reinterpret_cast<Dictionary*>(v);
				while (D != nullptr)
				{
					while (v->GetType() != eVarDictionary)
					{
						if (!val->GetNext(&v))
						{
							break;
						}
					}
					D = reinterpret_cast<Dictionary*>(v);

					if (D != nullptr)
					{
						key->SetDictionary(D);
						key->Process(tmp);
						O += tmp;
						tmp = "";
					}
					if (!val->GetNext(&v))
					{
						break;
					}
					D = reinterpret_cast<Dictionary*>(v);
				}
			}
			key->SetDictionary(d);
			rv = true;
		}

		return rv;
	}

	TemplateHandlers* DefaultHandlers = new TemplateHandlers({

		// `{{ variable } }` a single variable to replace;
		new TemplateHandler(
		new uint8_t[3]{ '{', '{', ' ' }, 3,
		new uint8_t[3]{ ' ', '}', '}' }, 3,
		new TemplateVariableHandler(&DefaultDoubleBraceHandler),
		0),

		// `{* filename_key list_key *}` includes the value of the string
		// variable `filename_key` and processes it as a `gte::Template`
		// with the list of dictionaries stored with the key `list_key`.
		new TemplateHandler(
		new uint8_t[3]{ '{', '*', ' ' }, 3,
		new uint8_t[3]{ ' ', '*', '}' }, 3,
		new TemplateVariableHandler(&DefaultAsteriskHandler),
		0),

		// `{# template_key #}` include a `gte::Template`
		// (stored using template_key) processing it using
		// its internal dictionary;
		new TemplateHandler(
		new uint8_t[3]{ '{', '#', ' ' }, 3,
		new uint8_t[3]{ ' ', '#', '}' }, 3,
		new TemplateVariableHandler(&DefaultHashHandler),
		0),
	});

#pragma endregion DefaultHandlers

#pragma region Variable
	Variable::Variable() :m_type(eVarNone)
	{

	}

	Variable::~Variable()
	{

	}

	eVariableType Variable::GetType()
	{
		return m_type;
	}
#pragma endregion Variable

#pragma region String
	String::String(std::string v) : m_value(v),
		Variable()
	{
		m_type = eVarString;
	}

	String::~String()
	{

	}

	void String::Set(std::string v)
	{
		m_value = v;
	}

	std::string String::Get()
	{
		return m_value;
	}

#pragma endregion String


#pragma region List
	List::List() : Variable()
	{
		m_type = eVarList;
		m_iterator = m_value.begin();
	}

	bool List::GetFirst(Variable **value)
	{
		m_iterator = m_value.begin();
		if (m_iterator != m_value.end())
		{
			*value = (*m_iterator);
			return true;
		}
		value = nullptr;
		return false;
	}

	bool List::GetNext(Variable **value)
	{
		if (m_iterator != m_value.end())
		{
			m_iterator++;
			if (m_iterator != m_value.end())
			{
				*value = (*m_iterator);
				return true;
			}
		}
		value = nullptr;
		return false;
	}

	List::~List()
	{
		m_iterator = m_value.begin();
		while (m_iterator != m_value.end())
		{
			m_iterator = m_value.erase(m_iterator);
		}
	}

	void List::Add(Variable *v)
	{
		m_value.push_back(v);
	}

#pragma endregion String


#pragma region Dictionary
	Dictionary::Dictionary() :
		Variable()
	{
		m_type = eVarDictionary;
	}

	Dictionary::~Dictionary()
	{
		if (m_value.size() > 0)
		{
			m_iterator = m_value.begin();
			while (m_iterator != m_value.end())
			{
				m_iterator = m_value.erase(m_iterator);
			}
		}
	}

	bool Dictionary::GetFirst(std::string &key, Variable **value)
	{
		m_iterator = m_value.begin();
		if (m_iterator != m_value.end())
		{
			key = m_iterator->first;
			*value = m_iterator->second;
			return true;
		}
		key = "";
		value = nullptr;
		return false;
	}

	bool Dictionary::GetNext(std::string &key, Variable **value)
	{
		if (m_iterator != m_value.end())
		{
			m_iterator++;
			if (m_iterator != m_value.end())
			{
				key = m_iterator->first;
				*value = m_iterator->second;
				return true;
			}
		}
		key = "";
		value = nullptr;
		return false;
	}

	void Dictionary::Set(std::string key, Variable *v)
	{
		m_value[key] = v;
	}

	bool Dictionary::GetByKey(std::string key, Variable **value)
	{
		if (m_value.find(key) != m_value.end())
		{
			*value = m_value[key];
			return true;
		}
		*value = nullptr;
		return false;
	}

	void Dictionary::Set(std::string k, std::string v)
	{
		m_value[k] = new String(v);
	}
#pragma endregion Dictionary


#pragma region TemplateHandler
	TemplateHandler::TemplateHandler(uint8_t *b, uint32_t a,
		uint8_t *e, uint32_t c,
		TemplateVariableHandler *h, uint8_t f) :
		token_beg(b), toksz_beg(a), token_end(e), toksz_end(c),
		handler(h), flags(f)
	{
	}

	TemplateHandler::~TemplateHandler()
	{
		delete token_beg;
		delete token_end;
		delete handler;
	}

#pragma endregion TemplateHandler


#pragma region Template
	void Template::PreProcess()
	{
		uint8_t *cp = m_buffer;
		uint8_t *ep = m_buffer + m_buffer_size;

		// Token open state
		uint8_t* ta = nullptr;

		// Token end state
		uint8_t* te = nullptr;

		// Iterators
		uint32_t i = 0;
		uint32_t j = 0;

		// Length of current token open/close
		uint32_t len = 0;

		while (cp != ep)
		{
			if (ta == nullptr)
			{
				for (j = 0; j < m_template_handlers->size(); j++)
				{
					// Set 'in-use'
					m_template_handlers->at(j)->flags |= 1;

					len = m_template_handlers->at(j)->toksz_beg;

					if ((cp + len) >= ep)
					{
						continue;
					}

					// Match byte by byte
					for (i = 0; i < len; i++)
					{
						if (cp[i] != m_template_handlers->at(j)->token_beg[i])
						{
							m_template_handlers->at(j)->flags ^= 1;
							break;
						}
					}

					// If still set, scan for the remainder
					if ((m_template_handlers->at(j)->flags & 1))
					{
						// Set the opening point
						ta = cp;

						// Skip the pointer
						cp += len;

						// Stop looking for alternatives
						break;
					}
				}

				// skip post-loop
				if (ta != nullptr)
				{
					continue;
				}
			}

			if (ta != nullptr)
			{
				for (j = 0; j < m_template_handlers->size(); j++)
				{
					// If it's in use, continue
					if ((m_template_handlers->at(j)->flags & 1))
					{
						len = m_template_handlers->at(j)->toksz_end;

						if ((cp + len) >= ep)
						{
							continue;
						}

						te = cp;

						for (i = 0; i < len; i++)
						{
							if (cp[i] != m_template_handlers->at(j)->token_end[i])
							{
								te = nullptr;
								break;
							}
						}

						// If it's cleared then the end of the variable has been found
						if (te != nullptr)
						{
							cp += len;

							// Store
							m_tokens.push_back(new TemplateVariable({
								j, ta, cp, std::string(
								ta + m_template_handlers->at(j)->toksz_beg,
								cp - m_template_handlers->at(j)->toksz_end
								)
							}));

							// Clear
							ta = nullptr;
							te = nullptr;

							// Stop looking for alternatives
							break;
						}
					}
				}

				// skip post-loop
				if (ta == nullptr)
				{
					continue;
				}
			}
			cp++;
		}
	}

	Template::Template(void *data, uint64_t length,
		TemplateHandlers* template_handlers) : Variable(),
		m_filename("")
	{
		m_type = eVarTemplate;
		m_variables = new Dictionary();

		m_buffer = new uint8_t[length + 1];
		memcpy(m_buffer, data, length);
		m_buffer_size = length;
		m_buffer[length] = 0;

		if (template_handlers == nullptr)
		{
			m_template_handlers = DefaultHandlers;
		}
		else
		{
			m_template_handlers = template_handlers;
		}

		PreProcess();
	}

	Template::Template(std::string filename,
		TemplateHandlers* template_handlers) : Variable(),
		m_filename(filename)
	{
		m_type = eVarTemplate;
		m_variables = new Dictionary();

		if (template_handlers == nullptr)
		{
			m_template_handlers = DefaultHandlers;
		}
		else
		{
			m_template_handlers = template_handlers;
		}

		// Open in binary form
		std::ifstream in(filename, std::ios::binary);

		if (in.is_open())
		{
			// Move to end
			in.seekg(0, in.end);

			// Get size
			m_buffer_size = in.tellg();

			// Move to start
			in.seekg(0, in.beg);

			// Create a buffer
			m_buffer = new uint8_t[m_buffer_size + 1];

			// Read to buffer
			in.read((char*)m_buffer, m_buffer_size);
			uint64_t read = in.gcount();

			in.close();

			PreProcess();
		}
	}

	Template::~Template()
	{
		delete m_buffer;

		std::vector<TemplateVariable*>::iterator it = m_tokens.begin();

		while (it != m_tokens.end())
		{
			it = m_tokens.erase(it);
		}

		if (m_variables != nullptr)
		{
			delete m_variables;
		}
	}

	void Template::SetVariable(std::string key,
		Variable *value)
	{
		m_variables->Set(key, value);
	}

	void Template::SetVariable(std::string key,
		std::string v)
	{
		m_variables->Set(key, new String(v));
	}

	Variable* Template::GetVariable(std::string key)
	{
		Variable *v = nullptr;
		if (m_variables->GetByKey(key, &v))
		{
			return v;
		}
		return nullptr;
	}

	bool Template::Process(std::ofstream &out)
	{
		bool rv = false;
		if (out.is_open())
		{
			uint8_t *curs = m_buffer;
			uint8_t *term = m_buffer + m_buffer_size;

			// Vector is stored in order, step through
			uint8_t *tkn0 = nullptr;
			uint8_t *tkn1 = nullptr;

			uint32_t pos = 0;
			uint32_t store_pos = 0;
			uint32_t nm_count = 0;

			std::string tmp = "";
			std::string key = "";
			Variable *var = nullptr;
			Variable **vars = nullptr;
			TemplateVariableHandler *hndl = nullptr;

			std::vector<TemplateVariable*>::iterator it = m_tokens.begin();
			while (it != m_tokens.end())
			{
				tkn0 = (*it)->var_start;
				tkn1 = (*it)->var_end;

				if (curs < tkn0)
				{
					// Anything remaining
					out << std::string(curs, tkn0);
				}

				// Repl
				hndl = m_template_handlers->at((*it)->m_type)->handler;
				if (hndl != nullptr)
				{
					uint32_t num_var = 0;
					std::vector<std::string> key_var;
					std::string nm = (*it)->name;

					while (nm.find(' ') != -1)
					{
						key_var.push_back(nm.substr(0, nm.find(" ")));
						nm = nm.substr(nm.find(" ") + 1);
					}

					if (nm.length() > 0)
					{
						key_var.push_back(nm);
					}

					pos = 0;
					store_pos = 0;
					nm_count = key_var.size();

					vars = (Variable**)calloc(nm_count, sizeof(Variable*));

					for (uint32_t i = 0; i < nm_count; i++)
					{
						if (m_variables->GetByKey(key_var[i], &vars[i]))
						{
							store_pos++;
						}
					}

					// Process
					if (nm_count == store_pos)
					{
						if ((*hndl)(*it, vars, tmp))
						{
							out << tmp;
							tmp = "";
						}
					}

					if (nm_count > 0)
					{
						free(vars);
						vars = nullptr;
					}
				}

				// Update
				curs = tkn1;

				it++;
			}

			if (curs < term)
			{
				out << std::string(curs, term);
			}
		}
		return rv;
	}

	void Template::SetDictionary(Dictionary *D)
	{
		m_variables = D;
	}

	Dictionary* Template::GetDictionary()
	{
		return m_variables;
	}

	bool Template::Process(std::string &out)
	{
		bool rv = true;

		uint8_t *curs = m_buffer;
		uint8_t *term = m_buffer + m_buffer_size;

		// Vector is stored in order, step through
		uint8_t *tkn0 = nullptr;
		uint8_t *tkn1 = nullptr;

		uint32_t pos = 0;
		uint32_t store_pos = 0;
		uint32_t nm_count = 0;

		std::string tmp = "";
		std::string key = "";
		Variable *var = nullptr;
		Variable **vars = nullptr;
		TemplateVariableHandler *hndl = nullptr;

		std::vector<TemplateVariable*>::iterator it = m_tokens.begin();
		while (it != m_tokens.end())
		{
			tkn0 = (*it)->var_start;
			tkn1 = (*it)->var_end;

			if (curs < tkn0)
			{
				// Anything remaining
				out += std::string(curs, tkn0);
			}

			// Repl
			hndl = m_template_handlers->at((*it)->m_type)->handler;
			if (hndl != nullptr)
			{
				uint32_t num_var = 0;
				std::vector<std::string> key_var;
				std::string nm = (*it)->name;

				while (nm.find(' ') != -1)
				{
					key_var.push_back(nm.substr(0, nm.find(" ")));
					nm = nm.substr(nm.find(" ") + 1);
				}

				if (nm.length() > 0)
				{
					key_var.push_back(nm);
				}

				pos = 0;
				store_pos = 0;
				nm_count = key_var.size();

				vars = (Variable**)calloc(nm_count, sizeof(Variable*));

				for (uint32_t i = 0; i < nm_count; i++)
				{
					if (m_variables->GetByKey(key_var[i], &vars[i]))
					{
						store_pos++;
					}
				}

				// Process
				if (nm_count == store_pos)
				{
					if ((*hndl)(*it, vars, tmp))
					{
						out += tmp;
						tmp = "";
					}
				}

				if (nm_count > 0)
				{
					free(vars);
					vars = nullptr;
				}
			}

			// Update
			curs = tkn1;

			it++;
		}

		if (curs < term)
		{
			out += std::string(curs, term);
		}

		return rv;
	}

	void Template::PrintTokens()
	{
		for (uint32_t i = 0; i < m_tokens.size(); i++)
		{
			printf("%i `%s`\n", i, m_tokens[i]->name.c_str());
		}
	}
} // namespace gte